#!/usr/bin/env python3

import json
import re

with open('missingPokemonMetadata.json') as f:
  data = json.loads(f.read())
  
new_pokes = []
pokemon = data.get('pokemon', [])
for poke in pokemon:
  template = poke['template_id']
  raw_name = template.split('_POKEMON_')[1]
  if re.search('20(19|20|21|22)$', raw_name):
    raw_name = raw_name.split('_')[0]
  name = raw_name.replace('_', '_').title()
  image_name = raw_name.lower().replace('_', '')
  emoji_name = raw_name.lower()
  new_poke = {
    "template_id": template,
    "name": name,
    "display_name": name,
    "image": image_name,
    "emoji": emoji_name,
    "pokebattler_name": raw_name
  }
  new_pokes.append(new_poke)
  
full = {'update': {'pokemon': new_pokes } }

with open('addition.json', 'w') as f:
  f.write(json.dumps(full, indent=4))
